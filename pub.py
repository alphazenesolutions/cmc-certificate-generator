from pubnub.callbacks import SubscribeCallback
from pubnub.enums import PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub

ENTRY = "CMC"
CHANNEL = "CMC_certificate_generator"
the_update = None

# replace the key placeholders with your own PubNub publish and subscribe keys
pnconfig = PNConfiguration()
pnconfig.publish_key = "pub-c-091a607a-438c-49ef-89c7-93056c1a5541"
pnconfig.subscribe_key = "sub-c-c64d8ca8-d350-11eb-b6c2-0298fc8e4944"
pnconfig.uuid = "sec-c-NGY3NzRmNTgtNjg2OS00MzJhLTk1MDMtMjZmZDE1NTJjZDRm"

pubnub = PubNub(pnconfig)


print("*****************************************")
print("* Submit updates to The Guide for Earth *")
print("*     Enter 42 to exit this process     *")
print("*****************************************")

while the_update != "42":
    name = input("Name : ")
    course = input("Course : ")
    id = input("Id: ")
    the_message = {"entry": ENTRY, "update": the_update, "name": name, "course": course, "id": id}
    envelope = pubnub.publish().channel(CHANNEL).message(the_message).sync()

    if envelope.status.is_error():
        print("[PUBLISH: fail]")
        print("error: %s" % envelope.status.error)
    else:
        print("[PUBLISH: sent]")
        print("timetoken: %s" % envelope.result.timetoken)
