import os
import pandas as pd
import firebase_admin

import datetime
from PIL import Image, ImageDraw, ImageFont
from firebase_admin import credentials
from firebase_admin import storage
from firebase_admin import firestore
from pubnub.callbacks import SubscribeCallback
from pubnub.enums import PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub

ENTRY = "CMC"
CHANNEL = "CMC_certificate_generator"



current_date = datetime.date.today().strftime("%d-%m-%Y")
data = current_date
print(data)
# replace the key placeholders with your own PubNub publish and subscribe keys
pnconfig = PNConfiguration()
pnconfig.publish_key = "pub-c-091a607a-438c-49ef-89c7-93056c1a5541"
pnconfig.subscribe_key = "sub-c-c64d8ca8-d350-11eb-b6c2-0298fc8e4944"
pnconfig.uuid = "sec-c-NGY3NzRmNTgtNjg2OS00MzJhLTk1MDMtMjZmZDE1NTJjZDRm"

pubnub = PubNub(pnconfig)

cred = credentials.Certificate('cmc-maritime-academy.json')
firebase_admin.initialize_app(cred, {
    'storageBucket': 'cmc-maritime-academy.appspot.com'
})

db = firestore.client()
bucket = storage.bucket()


class MySubscribeCallback(SubscribeCallback):
    def message(self, pubnub, event):
        print("[MESSAGE received]")

        if event.message["update"] == "42":
            print("The publisher has ended the session.")
            exit(0)
        else:     
            username = event.message["username"]
            email = event.message["email"]
            coursetitle = event.message["coursetitle"]
            coursesubtitle = event.message["coursesubtitle"]
            useruserid = event.message["useruserid"]
            courseid = event.message["courseid"]
            certificateid = event.message["certificateid"]            
           

            #Certificate Designing
            font = ImageFont.truetype('RacingSansOne-Regular.ttf',210)
            font1 = ImageFont.truetype('GlacialIndifference-Regular.otf',85)
            font2 = ImageFont.truetype('GlacialIndifference-Regular.otf',75)
            img = Image.open('certificate.jpg')
            draw = ImageDraw.Draw(img)
            draw.text(xy=(1168,855),text='{}'.format(username),fill="#99337f",font=font)
            draw.text(xy=(1855,1150),text='"{}"'.format(coursetitle),fill=(0,0,0),font=font1)
            draw.text(xy=(1982,1255),text='"{}"'.format(data),fill=(0,0,0),font=font2)
            
            #Creating Directory
            newcoursetitle = coursetitle.replace(" ", "")
            try:
                os.mkdir('certificates/'+useruserid)
                os.mkdir('certificates/'+useruserid+'/'+course)
            except:
                os.mkdir('certificates/'+useruserid+'/'+course)

            #Saving Image In Directory
            img.save('certificates/'+useruserid+'/'+newcoursetitle+'/certificate.jpg')
            url = 'certificates/'+useruserid+'/'+newcoursetitle+'/certificate.jpg'

            #Upload To Firebase
            fileName = url
            bucket = storage.bucket()
            blob = bucket.blob(fileName)
            blob.upload_from_filename(fileName)
            blob.make_public()
            print("your file url", blob.public_url)

            #Update URL
            doc_ref = db.collection(u'certificates').document(certificateid)
            doc_ref.update({
                    'certificate': blob.public_url,
                    'status':"Completed"
                })
           



    def presence(self, pubnub, event):
        print("[PRESENCE: {}]".format(event.event))
        print("uuid: {}, channel: {}".format(event.uuid, event.channel))

    def status(self, pubnub, event):
        if event.category == PNStatusCategory.PNConnectedCategory:
            print("[STATUS: PNConnectedCategory]")
            print("connected to channels: {}".format(event.affected_channels))

pubnub.add_listener(MySubscribeCallback())
pubnub.subscribe().channels(CHANNEL).with_presence().execute()

print("***************************************************")
print("* Waiting for updates to The Guide about {}... *".format(ENTRY))
print("***************************************************")
